<?php
declare(strict_types=1);

namespace App\Entity;

use App\Exception\InvalidDataException;

final class QuestionList
{
    /**
     * @var Question[]
     */
    private array $data;

    public function __construct()
    {
        $this->data = [];
    }

    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     * @return $this
     * @throws InvalidDataException
     */
    public function setData(array $data): self
    {
        if (!$this->validateData($data)) {
            throw new InvalidDataException('The data for QuestionList is invalid. Onli Question objects allowed');
        }

        $this->data = $data;

        return $this;
    }

    private function validateData(array $data): bool
    {
        if (empty($data)) {
            return true;
        }

        $filterData = array_filter(
            $data,
            function($item) { return $item instanceof Question; }
        );

        return count($filterData) === count($data);
    }
}