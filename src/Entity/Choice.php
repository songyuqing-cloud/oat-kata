<?php
declare(strict_types=1);

namespace App\Entity;

final class Choice
{
    private string $text;

    public function __construct()
    {
        $this->text = '';
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }
}