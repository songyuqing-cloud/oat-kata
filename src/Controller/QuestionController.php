<?php
declare(strict_types=1);

namespace App\Controller;

use App\Exception\MissingParameterException;
use App\Service\QuestionService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use App\Entity\QuestionList;
use App\Entity\Question;

class QuestionController
{
    public function __construct(private QuestionService $questionService)
    {
    }

    /**
     * Returns the list of translated questions and associated choices.
     *
     * @Route("/questions", methods={"GET"}, name="question_list")
     *
     * @OA\Response(
     *     response=200,
     *     description="List of translated questions and associated choices",
     *     @Model(type=QuestionList::class)
     * )
     * @OA\Parameter(
     *     name="lang",
     *     in="query",
     *     description="Language (ISO-639-1 code) in which the questions and choices should be translated",
     *     @OA\Schema(type="string")
     * )
     */
    public function getQuestions(Request $request): JsonResponse
    {
        $lang = $request->get('lang');

        if (empty($lang)) {
            $missingParameterException = new MissingParameterException('Lang is empty');
            return new JsonResponse($missingParameterException->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        try {
            $result = $this->questionService->getQuestionList($lang);
        } catch (\Throwable $exception) {
            return new JsonResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }

    /**
     * Creates a new question and associated choices (the number of associated choices must be exactly equal to 3)
     *
     * @Route("/questions", methods={"POST"}, name="question_post")
     *
     * @OA\Response(
     *     response=200,
     *     description="Question and associated choices (not translated)",
     *     @Model(type=Question::class)
     * )
     *
     * @OA\RequestBody(
     *     description="Pet object that needs to be added to the store",
     *     required=true,
     *     @OA\JsonContent(ref="#/components/schemas/Question")
     * )
     */
    public function postAction(Request $request)
    {
        try {
            $result = $this->questionService->postQuestion($request->getContent());
        } catch (\Throwable $exception) {
            return new JsonResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        return new JsonResponse($result, Response::HTTP_OK, [], true);
    }
}