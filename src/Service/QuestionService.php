<?php
declare(strict_types=1);

namespace App\Service;

use App\DataSource\InterfaceDataSource;
use App\Entity\Choice;
use App\Entity\Question;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

final class QuestionService
{
    private const QUESTION_TEXT_FIELD = 'text';
    private const QUESTION_CREATED_AT_FIELD = 'createdAt';
    private const QUESTION_CHOICES_FIELD = 'choices';

    public function __construct(private InterfaceDataSource $dataSource)
    {
    }

    /**
     * @param string $lang
     * @return string
     * @throws \Throwable
     */
    public function getQuestionList(string $lang): string
    {
        try {
            $questionList = $this->dataSource->getQuestionList($lang);

            return $this->encodeResult($questionList);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $content
     * @return string
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     * @throws \JsonException
     * @throws \Throwable
     */
    public function postQuestion(string $content): string
    {
        try {
            $questionRequest = json_decode($content, true, 512, JSON_THROW_ON_ERROR);

            $question = $this->getQuestion($questionRequest);

            $this->dataSource->postQuestion($question);

            return $this->encodeResult($question);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param mixed $data
     * @return string
     */
    private function encodeResult(mixed $data): string
    {
        $encoders = array(new JsonEncoder());
        $normalizers = array(new DateTimeNormalizer(), new GetSetMethodNormalizer());

        $serializer = new Serializer($normalizers, $encoders);
        $result = $serializer->serialize(
            $data,
            'json',
            [
                'datetime_format' => 'Y-m-d H:i:s',
            ]
        );

        return $result;
    }

    /**
     * @param array $questionRequest
     * @return Question
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     */
    private function getQuestion(array $questionRequest): Question
    {
        $question = new Question();
        $question->setText($questionRequest[self::QUESTION_TEXT_FIELD]);
        $question->setCreatedAt(\DateTime::createFromFormat('Y-m-d H:i:s', $questionRequest[self::QUESTION_CREATED_AT_FIELD]));
        $choices = [];
        foreach ($questionRequest[self::QUESTION_CHOICES_FIELD] as $choiceRequest) {
            $choice = new Choice();
            $choice->setText($choiceRequest[self::QUESTION_TEXT_FIELD]);
            $choices[] = $choice;
        }
        $question->setChoices($choices);
        return $question;
    }
}