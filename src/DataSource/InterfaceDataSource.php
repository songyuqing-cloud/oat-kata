<?php
declare(strict_types=1);

namespace App\DataSource;

use App\Entity\Question;
use App\Entity\QuestionList;

interface InterfaceDataSource
{
    public function getQuestionList(string $lang): QuestionList;

    public function postQuestion(Question $question): Question;
}