<?php
declare(strict_types=1);

namespace App\DataSource\Csv;

use App\DataSource\AbstractDataSource;
use App\DataSource\InterfaceDataSource;
use App\Entity\Choice;
use App\Entity\Question;
use App\Entity\QuestionList;
use App\Exception\FileNotExistsException;
use App\Exception\InvalidDataException;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class CsvDataSource extends AbstractDataSource implements InterfaceDataSource
{
    private const QUESTION_TEXT_HEADER = 'Question text';
    private const QUESTION_CREATED_AT_HEADER = 'Created At';
    private const QUESTION_CHOICE_1_HEADER = 'Choice 1';
    private const QUESTION_CHOICE_HEADER = 'Choice';
    private const QUESTION_CHOICE_3_HEADER = 'Choice 3';

    public function __construct(private string $file)
    {
        $this->file = sprintf('%s/%s', __DIR__, $file);
        parent::__construct($this->file);
    }

    /**
     * @param string $lang
     * @return QuestionList
     * @throws FileNotExistsException
     * @throws InvalidDataException
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     * @throws \Throwable
     */
    public function getQuestionList(string $lang): QuestionList
    {
        try {
            if (!file_exists($this->file)) {
                throw new FileNotExistsException('CSV file with data does not exists');
            }
            $content = $this->readFile($this->file);

            return $this->fromArray($content, $lang);
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $file
     * @return array
     */
    private function readFile(string $file): array
    {
        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);

        return $serializer->decode(file_get_contents($file), 'csv');
    }

    /**
     * @param string $file
     * @param QuestionList $questionList
     */
    protected function writeDataOnDataSource(string $file, QuestionList $questionList)
    {
        $encoders = array(new CsvEncoder());
        $format = 'csv';
        $context = ['datetime_format' => 'Y-m-d H:i:s', 'no_headers' => true];

        $result = $this->getDataToWrite($questionList, $encoders, $format, $context);
        $result = sprintf(
            '"%s","%s","%s","%s","%s"%s%s',
            self::QUESTION_TEXT_HEADER,
            self::QUESTION_CREATED_AT_HEADER,
            self::QUESTION_CHOICE_1_HEADER,
            self::QUESTION_CHOICE_HEADER,
            self::QUESTION_CHOICE_3_HEADER,
            PHP_EOL,
            $result
        );

        file_put_contents($file, $result);
    }

    /**
     * @param array $data
     * @param string $lang
     * @return QuestionList
     * @throws InvalidDataException
     * @throws \App\Exception\InvalidChoiceException
     * @throws \App\Exception\InvalidNumberChoicesException
     */
    private function fromArray(array $data, string $lang): QuestionList
    {
        /*try {
            $translate = new GoogleTranslate();
            $translate->setSource();
            $translate->setTarget($lang);

            $questionList = new QuestionList();
            $questions = [];
            foreach ($data as $questionItem) {
                $choices = [];
                $question = new Question();
                $question->setText($translate->translate($questionItem[self::QUESTION_TEXT_HEADER]));
                $question->setCreatedAt(\DateTime::createFromFormat('Y-m-d H:i:s', $questionItem[self::QUESTION_CREATED_AT_HEADER]));
                $choices[] = $this->getChoice($translate->translate($questionItem[self::QUESTION_CHOICE_1_HEADER]));
                $choices[] = $this->getChoice($translate->translate($questionItem[self::QUESTION_CHOICE_HEADER]));
                $choices[] = $this->getChoice($translate->translate($questionItem[self::QUESTION_CHOICE_3_HEADER]));
                $question->setChoices($choices);
                $questions[] = $question;
            }
            $questionList->setData($questions);

            return $questionList;
        } catch (\Throwable $exception) {
            throw $exception;
        }*/
        try {
            $questionList = new QuestionList();
            $questions = [];
            foreach ($data as $questionItem) {
                $choices = [];
                $question = new Question();
                $question->setText($questionItem[self::QUESTION_TEXT_HEADER]);
                $question->setCreatedAt(\DateTime::createFromFormat('Y-m-d H:i:s', $questionItem[self::QUESTION_CREATED_AT_HEADER]));
                $choices[] = $this->getChoice($questionItem[self::QUESTION_CHOICE_1_HEADER]);
                $choices[] = $this->getChoice($questionItem[self::QUESTION_CHOICE_HEADER]);
                $choices[] = $this->getChoice($questionItem[self::QUESTION_CHOICE_3_HEADER]);
                $question->setChoices($choices);
                $questions[] = $question;
            }
            $questionList->setData($questions);

            return $questionList;
        } catch (\Throwable $exception) {
            throw $exception;
        }
    }

    /**
     * @param string $text
     * @return Choice
     */
    private function getChoice(string $text): Choice
    {
        $choice = new Choice();
        $choice->setText($text);
        return $choice;
    }
}