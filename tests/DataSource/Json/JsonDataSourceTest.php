<?php
declare(strict_types=1);

namespace App\Tests\DataSource\Json;

use App\DataSource\Json\JsonDataSource;
use App\Entity\Question;
use App\Entity\QuestionList;
use App\Tests\Util\QuestionFactory;
use PHPUnit\Framework\TestCase;

final class JsonDataSourceTest extends TestCase
{
    /**
     * @test
     */
    public function json_data_source_get_question_list(): void
    {
        $jsonDataSource = new JsonDataSource('data/questions_test.json');
        $questionList = $jsonDataSource->getQuestionList('es');

        self::assertInstanceOf(QuestionList::class, $questionList);
        self::assertIsArray($questionList->getData());
    }

    /**
     * @test
     */
    public function json_data_source_post_question(): void
    {
        $question = QuestionFactory::getQuestion();

        $jsonDataSource = new JsonDataSource('data/questions_test.json');
        $questionListBefore = $jsonDataSource->getQuestionList('es');
        $question = $jsonDataSource->postQuestion($question);

        $questionListAfter = $jsonDataSource->getQuestionList('es');

        self::assertInstanceOf(Question::class, $question);
        self::assertEquals(count($questionListBefore->getData()) + 1, count($questionListAfter->getData()));
    }
}