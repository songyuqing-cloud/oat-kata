<?php
declare(strict_types=1);

namespace App\Tests\Controller;

use App\Tests\Util\QuestionFactory;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class QuestionControllerTest extends WebTestCase
{
    /**
     * @test
     */
    public function question_list()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('GET', $this->getRouter('question_list'), ['lang' => 'es']);

        $statusCode = $client->getResponse()->getStatusCode();

        self::assertTrue(Response::HTTP_OK === $statusCode);
    }

    /**
     * @test
     */
    public function question_list_exception()
    {
        self::ensureKernelShutdown();
        $client = static::createClient();

        $client->request('GET', $this->getRouter('question_list'), ['lang' => '']);

        $statusCode = $client->getResponse()->getStatusCode();

        self::assertTrue(Response::HTTP_BAD_REQUEST === $statusCode);
    }

    /**
     * @test
     */
    public function question_post()
    {
        $requestString = QuestionFactory::getQuestionString();

        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('POST', $this->getRouter('question_post'), [], [], [], $requestString);

        $statusCode = $client->getResponse()->getStatusCode();

        self::assertTrue(Response::HTTP_OK == $statusCode);
    }

    /**
     * @test
     */
    public function question_post_exception()
    {
        $requestString = QuestionFactory::getQuestionStringException();

        self::ensureKernelShutdown();
        $client = static::createClient();
        $client->request('POST', $this->getRouter('question_post'), [], [], [], $requestString);

        $statusCode = $client->getResponse()->getStatusCode();

        self::assertTrue(Response::HTTP_BAD_REQUEST === $statusCode);
    }

    private function getRouter(string $name, array $params = [])
    {
        self::ensureKernelShutdown();
        $client = static::createClient();
        $router = $client->getContainer()->get('router');

        return $router->generate($name, $params);
    }
}