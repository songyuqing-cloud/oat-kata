<?php
declare(strict_types=1);

namespace App\Tests\Service;

use App\DataSource\Csv\CsvDataSource;
use App\DataSource\Json\JsonDataSource;
use App\Exception\FileNotExistsException;
use App\Service\QuestionService;
use App\Tests\Util\QuestionFactory;
use PHPUnit\Framework\TestCase;

class QuestionServiceTest extends TestCase
{
    /**
     * @test
     */
    public function question_service_json_data_source(): void
    {
        $jsonDataSource = new JsonDataSource('data/questions_test.json');
        $questionService = new QuestionService($jsonDataSource);

        $questionJson = $questionService->getQuestionList('es');
        $questionData = json_decode($questionJson, true, 512, JSON_THROW_ON_ERROR);

        self::assertIsArray($questionData);
    }

    /**
     * @test
     */
    public function question_service_csv_data_source(): void
    {
        $csvDataSource = new CsvDataSource('data/questions_test.csv');
        $questionService = new QuestionService($csvDataSource);

        $questionJson = $questionService->getQuestionList('es');
        $questionData = json_decode($questionJson, true, 512, JSON_THROW_ON_ERROR);

        self::assertIsArray($questionData);
    }

    /**
     * @test
     */
    public function csv_throw_file_exception(): void
    {
        $csvDataSourceMock = $this->createMock(CsvDataSource::class);
        $csvDataSourceMock->method('getQuestionList')->willThrowException(new FileNotExistsException());
        $questionService = new QuestionService($csvDataSourceMock);

        self::expectException(FileNotExistsException::class);

        $questionList = $questionService->getQuestionList('es');
    }

    /**
     * @test
     */
    public function json_throw_file_exception(): void
    {
        $jsonDataSourceMock = $this->createMock(JsonDataSource::class);
        $jsonDataSourceMock->method('getQuestionList')->willThrowException(new FileNotExistsException());
        $questionService = new QuestionService($jsonDataSourceMock);

        self::expectException(FileNotExistsException::class);

        $questionList = $questionService->getQuestionList('es');
    }

    /**
     * @test
     */
    public function question_service_post_question(): void
    {
        $requestString = QuestionFactory::getQuestionString();
        $jsonDataSource = new JsonDataSource('data/questions_test.json');

        $questionListBefore = $jsonDataSource->getQuestionList('es');
        $questionService = new QuestionService($jsonDataSource);

        $questionJson = $questionService->postQuestion($requestString);
        $questionListAfter = $jsonDataSource->getQuestionList('es');
        $questionData = json_decode($questionJson, true, 512, JSON_THROW_ON_ERROR);

        self::assertIsArray($questionData);
        self::assertEquals(count($questionListBefore->getData()) + 1, count($questionListAfter->getData()));
    }
}